@extends('layout.main')

@section('title', 'Data Mahasiswa')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong> DATA MAHASISWA </strong></h3>
                            <div class="right">
                                <a href="{{ url('data_mhs/add') }}" class="btn btn-success btn-sm">
                                    <i class="fa fa-plus"></i> Add
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th>Kelas</th>
                                        <th>Prodi</th>
                                        <th>Fakultas</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->nama_mahasiswa }}</td>
                                            <td>{{ $item->nim_mahasiswa }}</td>
                                            <td>{{ $item->kelas_mahasiswa }}</td>
                                            <td>{{ $item->prodi_mahasiswa }}</td>
                                            <td>{{ $item->fakultas_mahasiswa }}</td>
                                            <td>
                                                <a href="{{ url('data_mhs/edit/' .$item->id) }}" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <form action="{{ url('data_mhs/' .$item->id) }}" method="post" class="d-lg-inline" onclick="return confirm('Yakin Ingin Hapus Data?')">
                                                    @method('delete')
                                                    @csrf
                                                    <button class="btn btn-danger btn-sm">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
